package RMI;
/**
 *
 * @Filename ControllerInterface.java
 *
 * @Version $Id: ControllerInterface.java,v 1.0 2013/12/09 04:30:00 $
 *
 * @Revisions
 *     Initial Revision
 *
 */

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * This is the Interface class that extends Remote Class
 * 
 * @author Deepak Mahajan
 * 
 */

public interface ControllerInterface extends Remote {
	/**
	 * This method reflects the changes caused by second player. It sends 
	 * the value clicked by one player to other and displays it and throws
	 * RemoteException.
	 * 
	 * @param btNum button index
	 * 
	 * @param random random number
	 */

	public void myMouseI(Integer btNum, Integer random,Boolean flag) 
			throws RemoteException;

}
