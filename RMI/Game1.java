package RMI;

/**
 *
 * @Filename Game1.java
 *
 * @Version $Id: Game1.java,v 1.0 2013/12/9 05:09:00 $
 *
 * @Revisions
 *     Initial Revision
 *
 */

import java.rmi.AlreadyBoundException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

/**
 * This is the Game class that contains the RMI connection. It extends
 * UnicastRemoteObject and implements ControllerInterface
 * 
 * 
 * @author Deepak Mahajan
 * 
 */
public class Game1 extends UnicastRemoteObject implements ControllerInterface {
	/**
	 * Default constructor
	 * 
	 * @throws RemoteException
	 */
	protected Game1() throws RemoteException {
		super();
		// TODO Auto-generated constructor stub
	}

	private static final long serialVersionUID = 1L;
	// Object of Controller1 class are initialized
	static Controller1 c;
	// Object of ControllerInterface is initialized
	static ControllerInterface client;

	/**
	 * This is the main() method.
	 * 
	 * @param args
	 * @throws RemoteException
	 * @throws AlreadyBoundException
	 * @throws NotBoundException
	 */
	public static void main(String args[]) throws RemoteException,
			AlreadyBoundException, NotBoundException {

		if (args.length == 0)
			try {
				// Boolean flag is initialized as false
				boolean flag = false;
				// Server is created
				ControllerInterface server = new Game1();
				// A registry is created with port number 12345 for the
				// object of server
				Registry registry = LocateRegistry.createRegistry(14352);
				// Object of server is binded to the registry
				registry.rebind("server", server);

				while (!flag) {
					// New registry is created with port number 12346 to
					// lookup the object of client object
					Registry registry2 = LocateRegistry.getRegistry(
							"localhost", 14343);

					try {
						// Object client looks up the object of client class
						client = (ControllerInterface) registry2
								.lookup("client");
						flag = true;
					} catch (Exception ex) {
					}
				}
				// Object of controller is given a memory with 3 parameters
				c = new Controller1("Server Player", client, true);

			} catch (Exception e) {

			}

		else
			try {
				// Looks up the object of server in the registry
				Registry registry = LocateRegistry.getRegistry(args[0], 14352);
				// client got the object of server where he can make changes i.e
				// server
				ControllerInterface server = (ControllerInterface) registry
						.lookup("server");

				ControllerInterface client = new Game1();
				// Creates Registry for the object of Client
				Registry registry2 = LocateRegistry.createRegistry(14343);
				// Binds the object of client class with registry
				registry2.rebind("client", client);
				System.out.println("Server2 bound in registry");

				// the object "server" is pass to the constructor of client
				// where
				// game will be load
				c = new Controller1("Client Player", server, false);

			} catch (Exception ex) {

			}
	}

	@Override
	/**
	 * This method reflects the changes caused by second player. It sends the
	 * value clicked by one player to other and displays it and throws
	 * RemoteException.
	 * 
	 * @param btNum button index
	 * 
	 * @param random random number
	 */
	public void myMouseI(Integer btNum, Integer random, Boolean flag)
			throws RemoteException {
		c.myMouse(btNum, random, !flag);
	}

}