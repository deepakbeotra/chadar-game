package RMI;

/**
 *
 * @Filename Contoller1.java
 *
 * @Version $Id: Controller1.java,v 1.0 2013/12/9 05:45:00 $
 *
 * @Revisions
 *     Initial Revision
 *
 */

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.Serializable;
import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

/**
 * This is the Controller class. It has the logic and view of the game. It
 * extends JFrame for the GUI, implements Mouselistener and Serializable to
 * perform actions using mouse only and to send objects over the network
 * Respectively.
 * 
 * @author Deepak Mahajan
 * 
 */

public class Controller1 extends JFrame implements MouseListener, Serializable {

	// contentPane contains 110 buttons.
	private JPanel contentPane;

	// scorePane contains labels to print score of players
	// and the current state of game and which player is playing.
	private JPanel scorePane;

	// mainPanel contains scorePane and contentPane
	private JPanel mainPanel;

	private JLabel player1, player2;

	// labels to print scores of players.
	private JLabel score1, score2;

	JButton bt[] = new JButton[70];

	// labels to describe which player is playing
	private JLabel cursor1;
	private JLabel cursor2;

	// label to describe current state of game
	private JLabel info;

	// variable to store value of random number
	// which is caught on network
	Integer random;

	String name;

	// variable used to close the application
	Boolean exit = false;
	Boolean flag;

	// Object of class Model1 is initialized
	Model1 m;

	// Object of class ControllerInterface is initialized
	ControllerInterface obj1;

	/**
	 * This is a parameterized constructor.
	 * 
	 * @param name
	 * @param obj1
	 * @param flag
	 */

	public Controller1(String name, ControllerInterface obj1, Boolean flag) {
		this.obj1 = obj1;
		this.name = name;
		setTitle(this.name);
		design();
		setVisible(true);
		m = new Model1();
		this.flag = flag;

	}

	/**
	 * 
	 * This method defines the GUI of the game using Swings. It uses JFrame,
	 * Jlabel, Jpanel and JButton.
	 * 
	 */

	public void design() {
		// super("Match The Numbers");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// sets the size of the frame.
		setBounds(30, 30, 900, 700);

		contentPane = new JPanel();
		scorePane = new JPanel();

		// sets the border of the panel,contentPane
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		// sets the layout of the contentPane
		contentPane.setLayout(new GridLayout(7, 10));

		// // sets the border of the panel,scorePane
		scorePane
				.setBorder(new TitledBorder(new EtchedBorder(
						EtchedBorder.LOWERED, null, null),
						"Deepak And Vanya's Game", TitledBorder.CENTER,
						TitledBorder.TOP, null, new Color(0, 0, 0)));
		// sets the layout of the scorePane
		scorePane.setLayout(new GridLayout(1, 4));

		// sets the layout of the scorePane for the server
		if ((this.name).equals("Server Player")) {
			player1 = new JLabel("My Score");
			player1.setHorizontalAlignment(SwingConstants.CENTER);
			player2 = new JLabel("Client Player");
			player2.setHorizontalAlignment(SwingConstants.CENTER);
			info = new JLabel("Your Turn");
			info.setHorizontalAlignment(SwingConstants.CENTER);
		} else {
			// sets the layout of the scorePane for the Client side
			player1 = new JLabel("Server Player");

			player1.setHorizontalAlignment(SwingConstants.CENTER);
			player2 = new JLabel("My Score");
			player2.setHorizontalAlignment(SwingConstants.CENTER);
			info = new JLabel("Opponent's Turn");
			info.setHorizontalAlignment(SwingConstants.CENTER);
		}

		score1 = new JLabel("0");
		score1.setHorizontalAlignment(SwingConstants.CENTER);

		score2 = new JLabel("0");
		score2.setHorizontalAlignment(SwingConstants.CENTER);

		cursor1 = new JLabel("===>>>");
		cursor1.setHorizontalAlignment(SwingConstants.TRAILING);

		// add components to scorePane
		scorePane.add(cursor1);
		scorePane.add(player1);
		scorePane.add(score1);

		scorePane.add(info);

		cursor2 = new JLabel("");
		cursor2.setHorizontalAlignment(SwingConstants.TRAILING);
		scorePane.add(cursor2);
		scorePane.add(player2);
		scorePane.add(score2);

		// creates the 110 buttons
		for (Integer i = 0; i < 70; i++) {
			bt[i] = new JButton(Integer.toString(i));
			bt[i].addMouseListener(this);
			contentPane.add(bt[i]);
		}

		// to add elements in the randomNumbers arrayList
		mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));

		// add scorePane and contentPane to mainPanel
		mainPanel.add(scorePane);
		mainPanel.add(contentPane);
		getContentPane().add(mainPanel);

	}

	/**
	 * This method is invoked when a player clicks a button
	 * 
	 * @param btNum
	 *            index of the button
	 * @param random
	 *            random number
	 * @param flag
	 *            true/false
	 */

	public void myMouse(Integer btNum, Integer random, Boolean flag) {

		// temporary button object referring to the
		// current button being clicked
		JButton tempButton = bt[btNum];

		this.random = random;
		if (!tempButton.isEnabled()) {
			return;
		} else {
			this.flag = flag;

			// To set the chance of player
			m.setCountForTurn();
			if (m.getCountForTurn() % 2 == 0) {
				cursor1.setText("===>>>");
				cursor2.setText("");

			} else {
				cursor1.setText("");
				cursor2.setText("===>>>");

			}

			// boolean variable to check the number generated
			// is already present or not
			this.random = random;
			m.removeNumber(random);
			tempButton.setText(Integer.toString(random));

			// disabled the clicked button
			tempButton.setEnabled(false);
			tempButton.setOpaque(true);

			// Set the color of button when clicked
			tempButton.setBackground(Color.LIGHT_GRAY);
			tempButton.setFont(new Font("Serif", Font.BOLD, 14));

			// In case of Match found of random number
			if (m.checkValue(random)) {
				info.setText("Match Found Of " + random);
				Integer playerWin = m.getCountForTurn() % 2;
				if (playerWin == 1) {
					m.setPlayer1Score();
					score1.setText(Integer.toString(m.getPlayer1Score()));
				} else {
					m.setPlayer2Score();
					score2.setText(Integer.toString(m.getPlayer2Score()));
				}

			} else {
				info.setText("Game Running");
			}
			m.addNumber(random);

			if ((player1.getText()).equals("My Score")
					&& m.getCountForTurn() == 70) {

				// To display the winner and looser on labels
				if (m.getPlayer1Score() > m.getPlayer2Score()) {
					cursor1.setText("WINNER");
					cursor2.setText("LOOSER");
					info.setText("You Won");

					// creates the showMessageDialog box if player1 wins
					JOptionPane.showMessageDialog(
							this,
							"Congratulation!!! You Won by "
									+ (m.getPlayer1Score() - m
											.getPlayer2Score()) + " points");
					System.exit(0);
				} else {
					cursor1.setText("LOOSER");
					cursor2.setText("WINNER");
					info.setText("You Lost");

					// creates the showMessageDialog box if player2 wins
					JOptionPane.showMessageDialog(this, "Sorry!!! You Lost by "
							+ (m.getPlayer2Score() - m.getPlayer1Score())

							+ " points");
					System.exit(0);
				}

			}

		}

	}

	@Override
	/**
	 * This method overrides the mouseClicked class
	 * and performs the functions on mouseClicked.
	 * 
	 * @param e
	 * 
	 */
	public void mouseClicked(MouseEvent e) {

		if (flag) {
			// tempButton stores the Source of the button clicked
			JButton tempButton = (JButton) e.getSource();

			// buttonNum passes the value of the button text on the network
			Integer buttonNum = Integer.parseInt(tempButton.getText());
			if (!tempButton.isEnabled()) {
				return;
			} else {
				flag = false;

				// Sets the turn
				m.setCountForTurn();
				if (m.getCountForTurn() % 2 == 0) {
					cursor1.setText("===>>>");
					cursor2.setText("");

				} else {
					cursor1.setText("");
					cursor2.setText("===>>>");

				}

				this.random = m.randomNumber();

				// remove the random number, generated in the arraylist
				m.removeNumber(random);
				tempButton.setText(Integer.toString(random));

				// disabled the clicked button
				tempButton.setEnabled(false);
				tempButton.setOpaque(true);
				tempButton.setBackground(Color.RED);
				tempButton.setFont(new Font("Serif", Font.BOLD, 14));

				// tempButton.setForeground(Color.RED);
				// Checks whether the number generated is previously present
				// or not
				if (m.checkValue(random)) {

					// If match is found displays the set
					info.setText("Match Found Of " + random);
					Integer playerWin = m.getCountForTurn() % 2;
					if (playerWin == 1) {

						// Sets the score of Player1
						m.setPlayer1Score();
						score1.setText(Integer.toString(m.getPlayer1Score()));
					} else {

						// Sets the score of Player2
						m.setPlayer2Score();
						score2.setText(Integer.toString(m.getPlayer2Score()));
					}

				} else {
					info.setText("Game Running");
				}

				// adds the random number generated into the arraylist
				m.addNumber(random);

				// creates the showMessageDialog box if player1 wins
				if ((player1.getText()).equals("Server Player")
						&& m.getCountForTurn() == 70) {
					if (m.getPlayer1Score() > m.getPlayer2Score()) {
						cursor1.setText("WINNER");
						cursor2.setText("LOOSER");
						info.setText("You Lost");

						// creates the showMessageDialog box if player1 wins
						JOptionPane
								.showMessageDialog(
										this,
										"Sorry!!! You Lost By "
												+ (m.getPlayer1Score() - m
														.getPlayer2Score())
												+ " points");
						exit = true;
					} else {
						cursor1.setText("LOOSER");
						cursor2.setText("WINNER");
						info.setText("You Won");

						// creates the showMessageDialog box if player2 wins
						JOptionPane
								.showMessageDialog(
										this,
										"You Won by "
												+ (m.getPlayer2Score() - m
														.getPlayer1Score())
												+ " points");
						exit = true;
					}

				}

				try {
					this.flag = false;

					// Passes the value to the object which is going to be
					// looked up by the other machine
					obj1.myMouseI(buttonNum, random, flag);

				} catch (Exception e1) {

				}
				if (exit) {
					System.exit(0);
				}

			}
		}

	}

	/**
	 * This method overrides the mouseEntered class
	 * 
	 * @param e
	 *            not used
	 * 
	 */

	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	/**
	 * This method overrides the mouseExited class
	 * 
	 * @param e  not used
	 * 
	 */
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	/**
	 * This method overrides the mousePressed class
	 * 
	 * @param e  not used
	 * 
	 */
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	/**
	 * This method overrides the mouseReleased class
	 * 
	 * @param e  not used
	 * 
	 */
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
