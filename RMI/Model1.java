package RMI;

/**
 *
 * @Filename Model1.java
 *
 * @Version $Id: Model1.java,v 1.0 2013/12/9 04:42:00 $
 *
 * @Revisions
 *     Initial Revision
 *
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

/**
 * This is the Model class that contains the Logic of the game. It implements
 * Serializable as we have to send objects to other systems
 * 
 * @author Deepak Mahajan
 * 
 */

public class Model1 implements Serializable {

	// Initializing ArrayList of numbers
	private ArrayList<Integer> numbers;

	// Initializing ArrayList of RandomNumbers generated
	private ArrayList<Integer> randomNumbers;

	// variable to store the count for turn
	private Integer countForTurn = 0;

	// to store the score of player1
	Integer player1WinCount = 0;

	// to store the score of player2
	Integer player2WinCount = 0;

	/**
	 * 
	 * Default Constructor
	 * 
	 */

	Model1() {

		// New Memory is given to ArrayList randomNumbers
		randomNumbers = new ArrayList<Integer>();

		// New Memory is given to ArrayList numbers
		numbers = new ArrayList<Integer>();
		setRNList();
	}

	/**
	 * This method adds random number to the ArrayList.
	 * 
	 */

	public void setRNList() {
		for (Integer i = 1; i <= 35; i++) {
			randomNumbers.add(i);
			randomNumbers.add(i);
		}
	}

	/**
	 * This method increments the count of turn.
	 * 
	 */

	public void setCountForTurn() {
		countForTurn++;
	}

	/**
	 * This method fetch countForTurn
	 * 
	 * @return countForTurn
	 */

	public Integer getCountForTurn() {
		return countForTurn;
	}

	/**
	 * This method addNumbers to the number ArrayList
	 * 
	 * @param random
	 *            Random Number generated
	 * 
	 */

	public void addNumber(Integer random) {
		numbers.add(random);
	}

	/**
	 * This method creates Random Number.
	 * 
	 * @return random
	 */

	public Integer randomNumber() {
		Random rd = new Random();
		Integer tempRandom = rd.nextInt(randomNumbers.size());
		Integer random = randomNumbers.get(tempRandom);
		return random;
	}

	/**
	 * This method prints the number in array list
	 * 
	 * @return numbers
	 */

	public ArrayList<Integer> printNumbers() {
		return numbers;
	}

	/**
	 * This method removes the random number from the arrayList random
	 * 
	 * @param a
	 */

	public void removeNumber(Integer a) {
		Integer random = a;
		randomNumbers.remove(random);
	}

	/**
	 * This method checks whether the number has been clicked previously or not.
	 * 
	 * @param random
	 * @return true/false
	 */

	public boolean checkValue(Integer random) {
		// Iterator is initialized
		Iterator<Integer> itr = numbers.iterator();
		while (itr.hasNext()) {
			Integer a = itr.next();
			if (a.equals(random))
				return true;

		}
		return false;
	}

	/**
	 * This method setInfo
	 * 
	 * @param random
	 */

	public void setInfo(Integer random) {

	}

	/**
	 * This method increments the score of Player1
	 * 
	 */

	public void setPlayer1Score() {
		player1WinCount++;

	}

	/**
	 * This method return the Player1Score
	 * 
	 * @return player1WinCount
	 * 
	 */

	public Integer getPlayer1Score() {
		return player1WinCount;
	}

	/**
	 * This method increments the score of Player2
	 * 
	 */

	public void setPlayer2Score() {
		player2WinCount++;
	}

	/**
	 * This method return the Player2Score
	 * 
	 * @return player2WinScore
	 */

	public Integer getPlayer2Score() {
		return player2WinCount;
	}
}
