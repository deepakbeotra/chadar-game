package socketProgramming;

//server
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Game1 {
	static ServerSocket ss;
	static Socket s;
	static ObjectInputStream in;
	static ObjectOutputStream out;

	public static void main(String args[]) {
		if (args.length == 0) {
			try {
				ss = new ServerSocket(12345);
				System.out.println("Server Started");
				s = ss.accept();
				System.out.println("Client Connected");

				out = new ObjectOutputStream(s.getOutputStream());
				in = new ObjectInputStream(s.getInputStream());
				System.out.println("Streams Connected");
			} catch (IOException e) {
				e.printStackTrace();
			}
			Controller1 c = new Controller1(out, "Server");
			while (true) {
				try {
					Integer btNum = (Integer) in.readObject();
					Integer i = (Integer) in.readObject();
					c.myMouse(btNum, i);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}

		}

		else {
			try {

				s = new Socket(args[0], 12345);
				in = new ObjectInputStream(s.getInputStream());
				out = new ObjectOutputStream(s.getOutputStream());
				System.out.println("client Running... ");
			} catch (Exception e) {
				e.printStackTrace();
			}
			Controller1 c = new Controller1(out, "Client");
			while (true) {
				try {
					Integer btNum = (Integer) in.readObject();
					Integer random = (Integer) in.readObject();
					c.myMouse(btNum, random);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

}