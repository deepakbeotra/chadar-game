package socketProgramming;

//server
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class Controller1 extends JFrame implements MouseListener, Serializable {

	// contentPane contains 110 buttons.
	private JPanel contentPane;

	// scorePane contains labels to print score of players
	// and the current state of game and which player is playing.
	private JPanel scorePane;

	// mainPanel contains scorePane and contentPane
	private JPanel mainPanel;

	private JLabel player1, player2;

	// labels to print scores of players.
	private JLabel score1, score2;

	JButton bt[] = new JButton[70];

	// labels to describe which player is playing
	private JLabel cursor1;
	private JLabel cursor2;

	// label to describe current state of game
	private JLabel info;

	static ObjectOutputStream pw;

	Integer random;

	Boolean flag = true;
	// Boolean me=true;
	Model1 m;

	public Controller1(ObjectOutputStream br, String name) {
		this.pw = br;
		design();
		setTitle(name);
		setVisible(true);
		m = new Model1();

	}

	public void design() {
		// super("Match The Numbers");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		// sets the size of the frame.
		setBounds(30, 30, 900, 700);

		contentPane = new JPanel();
		scorePane = new JPanel();

		// sets the border of the panel,contentPane
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		// sets the layout of the contentPane
		contentPane.setLayout(new GridLayout(7, 10));

		// // sets the border of the panel,scorePane
		scorePane
				.setBorder(new TitledBorder(new EtchedBorder(
						EtchedBorder.LOWERED, null, null),
						"Deepak And Vanya's Game", TitledBorder.CENTER,
						TitledBorder.TOP, null, new Color(0, 0, 0)));
		// sets the layout of the scorePane
		scorePane.setLayout(new GridLayout(1, 4));
		player1 = new JLabel("Player 1");
		player1.setHorizontalAlignment(SwingConstants.CENTER);
		score1 = new JLabel("0");
		score1.setHorizontalAlignment(SwingConstants.CENTER);
		player2 = new JLabel("Player 2");
		player2.setHorizontalAlignment(SwingConstants.CENTER);
		score2 = new JLabel("0");
		score2.setHorizontalAlignment(SwingConstants.CENTER);

		cursor1 = new JLabel("===>>>");
		cursor1.setHorizontalAlignment(SwingConstants.TRAILING);

		// add components to scorePane
		scorePane.add(cursor1);
		scorePane.add(player1);
		scorePane.add(score1);

		info = new JLabel("Game Start");
		info.setHorizontalAlignment(SwingConstants.CENTER);
		scorePane.add(info);

		cursor2 = new JLabel("");
		cursor2.setHorizontalAlignment(SwingConstants.TRAILING);
		scorePane.add(cursor2);
		scorePane.add(player2);
		scorePane.add(score2);

		// creates the 110 buttons

		for (Integer i = 0; i < 70; i++) {
			bt[i] = new JButton(Integer.toString(i));
			bt[i].addMouseListener(this);
			contentPane.add(bt[i]);
		}

		// to add elements in the randomNumbers arrayList

		mainPanel = new JPanel();
		mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));

		// add scorePane and contentPane to mainPanel
		mainPanel.add(scorePane);
		mainPanel.add(contentPane);
		getContentPane().add(mainPanel);

	}

	public void myMouse(Integer btNum, Integer random) {
		// temporary button object referring to the
		// current button being clicked
		// System.out.println(e);

		JButton tempButton = bt[btNum];
		this.random = random;
		if (!tempButton.isEnabled()) {
			return;
		} else {
			flag = true;

			// if(me)
			// {
			// player1.setText("My Score");
			// }
			// me=false;
			m.setCountForTurn();
			if (m.getCountForTurn() % 2 == 0) {
				cursor1.setText("===>>>");
				cursor2.setText("");

			} else {
				cursor1.setText("");
				cursor2.setText("===>>>");

			}
			// boolean variable to check the number generated
			// is already present or not
			this.random = random;
			m.removeNumber(random);
			tempButton.setText(Integer.toString(random));

			// disabled the clicked button
			tempButton.setEnabled(false);
			tempButton.setOpaque(true);
			tempButton.setBackground(Color.RED);
			if (m.checkValue(random)) {
				info.setText("Match Found Of " + random);
				Integer playerWin = m.getCountForTurn() % 2;
				if (playerWin == 1) {
					m.setPlayer1Score();
					score1.setText(Integer.toString(m.getPlayer1Score()));
				} else {
					m.setPlayer2Score();
					score2.setText(Integer.toString(m.getPlayer2Score()));
				}

			} else {
				info.setText("Game Running");
			}
			m.addNumber(random);
			if (m.getCountForTurn() == 70) {
				if (m.getPlayer1Score() > m.getPlayer2Score()) {
					cursor1.setText("WINNER");
					cursor2.setText("LOOSER");
					info.setText("Game Ends");

					// creates the showMessageDialog box if player1 wins
					JOptionPane.showMessageDialog(
							this,
							"Congratulations!!!! Player 1 Won by "
									+ (m.getPlayer1Score() - m
											.getPlayer2Score()) + " points");
					System.exit(0);
				} else {
					cursor1.setText("LOOSER");
					cursor2.setText("WINNER");
					info.setText("Game Ends");

					// creates the showMessageDialog box if player2 wins
					JOptionPane.showMessageDialog(
							this,
							"Congratulations!!!! Player 2 Won by "
									+ (m.getPlayer2Score() - m
											.getPlayer1Score()) + " points");
					System.exit(0);
				}

			}

		}

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// temporary button object referring to the
		// current button being clicked
		if (flag) {

			JButton tempButton = (JButton) e.getSource();
			Integer buttonNum = Integer.parseInt(tempButton.getText());
			if (!tempButton.isEnabled()) {
				return;
			} else {
				flag = false;
				m.setCountForTurn();
				if (m.getCountForTurn() % 2 == 0) {
					cursor1.setText("===>>>");
					cursor2.setText("");

				} else {
					cursor1.setText("");
					cursor2.setText("===>>>");

				}
				// boolean variable to check the number generated
				// is already present or not

				this.random = m.randomNumber();
				m.removeNumber(random);
				tempButton.setText(Integer.toString(random));

				// disabled the clicked button
				tempButton.setEnabled(false);
				tempButton.setOpaque(true);
				tempButton.setBackground(Color.RED);
				if (m.checkValue(random)) {
					info.setText("Match Found Of " + random);
					Integer playerWin = m.getCountForTurn() % 2;
					if (playerWin == 1) {
						m.setPlayer1Score();
						score1.setText(Integer.toString(m.getPlayer1Score()));
					} else {
						m.setPlayer2Score();
						score2.setText(Integer.toString(m.getPlayer2Score()));
					}

				} else {
					info.setText("Game Running");
				}
				m.addNumber(random);
				try {
					pw.writeObject(buttonNum);
					pw.writeObject(random);
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				if (m.getCountForTurn() == 70) {
					if (m.getPlayer1Score() > m.getPlayer2Score()) {
						cursor1.setText("WINNER");
						cursor2.setText("LOOSER");
						info.setText("Game Ends");

						// creates the showMessageDialog box if player1 wins
						JOptionPane
								.showMessageDialog(
										this,
										"Congratulations!!!! Player 1 Won by "
												+ (m.getPlayer1Score() - m
														.getPlayer2Score())
												+ " points");
						System.exit(0);
					} else {
						cursor1.setText("LOOSER");
						cursor2.setText("WINNER");
						info.setText("Game Ends");

						// creates the showMessageDialog box if player2 wins
						JOptionPane.showMessageDialog(
								this,
								"Congratulations!!!! Player 2 Won by "
										+ (m.getPlayer2Score() - m
												.getPlayer1Score())

										+ " points");
						System.exit(0);
					}

				}

			}
		}

	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

}
